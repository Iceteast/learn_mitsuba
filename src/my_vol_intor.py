#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import mitsuba as mi
import matplotlib.pyplot as plt
import platform as pf

if pf.platform()[:7] == "Windows":
    mi.set_variant('cuda_ad_rgb')
else:
    mi.set_variant('llvm_ad_rgb')

class MyVolIntegrator(mi.CppADIntegrator):
    
    def __init__(self, props):
        super().__init__(props)
        self.loopsize = props['loopsize']

    def sample(self,
            #    mode: dr.ADMode,
               scene: mi.Scene,
               sampler: mi.Sampler,
               ray: mi.Ray3f,
            #    reparam,
               active=True,
               **kwargs # Absorbs unused arguments
    ):
        gray_degree = mi.Float32(0)
        si : mi.SurfaceInteraction3f = scene.ray_intersect(ray)

        frame = mi.Frame3f(si.n)
        for _ in range(self.loopsize):
            x, y = sampler.next_2d()
            x = ((x - 0.5) * 2) ** 1
            y = ((y - 0.5) * 2) ** 1
            random_ray_local = mi.Vector3f(0, x, y)
            random_ray_world = si.spawn_ray(frame.to_world(random_ray_local)) 
            gray_degree[scene.ray_test(random_ray_world)] += 1
        return mi.Color3f(gray_degree / self.loopsize), active, None

# class MyAOVIntegrator(mi.MonteCarloIntegrator):
    # def __init__(self, props):
    #     print("init start")
    #     super(mi.MonteCarloIntegrator, self).__init__(props)
    #     print("init finished")

    # # try direct first

    # def sample(self, scene: mi.Scene, sampler, ray: mi.RayDifferential3f, medium: mi.Medium = None, active: bool = True):
    #     # -> Tuple[mi.Color3f, bool, List[float]]
    #     radiance = mi.Color3f(1, 1, 1)

    #     if medium is not None:
    #         return radiance * 0, True, [0.6]
    #     return radiance * 0.4, active, [0.3]

    # def render(self, scene: mi.Scene, sensor: mi.Sensor, seed: int = 0, spp: int = 0, develop: bool = True, evaluate: bool = True):
    #     film : mi.Film = sensor.film()
    #     w, h = film.width, film.height
    #     # cam_origin = scene.sensors
    #     # cam_dir = sensor.ptr
    #     random_pool = mi.PCG32(size=spp, initstate=seed)
    #     img : mi.TensorXf = mi.TensorXf(shape=w * h)
    #     for i in range(w):
    #         for j in range(h):
    #             ray_dir = mi.Frame3f(scene.bbox.center).to_world()
    #             ray = mi.Ray3f(o=cam_origin, d=ray_dir)
    #             si : mi.SurfaceInteraction3f = scene.ray_intersect(ray)
    #             indexEmitter, weight, sample = scene.sample_emitter(random_pool.next_float32())
    #             img.array[j + i * w] = scene.emitters[indexEmitter].eval(si) * weight
    #     return img
        
if __name__ == '__main__':

    mi.register_integrator("myaov", lambda props: MyVolIntegrator(props))
   
    spp = 128
    scene = mi.load_dict(mi.cornell_box())

    pt = mi.load_dict({
        'type' : 'path'
    })

    my = mi.load_dict({
        'type' : 'myaov',
        'loopsize' : 2,
    })

    aov = mi.load_dict({
        'type': 'aov',
        'aovs': 'dd.y:depth,nn:geo_normal',
        # 'my_image': {
        #     'type': 'path',
        # }
    })

    img = mi.render(scene, integrator=my, spp=spp)
    img_p = mi.render(scene, integrator=pt, spp=spp)
    img_a = mi.render(scene, integrator=aov, spp=spp)
    fig = plt.figure(figsize=(10, 6))
    fig.subplots_adjust(wspace=0.1)

    fig.add_subplot(1, 3, 1).imshow(img ** (1.0 / 2.2))
    plt.axis("off")
    plt.title('my AOV')
    fig.add_subplot(1, 3, 2).imshow(img_a[:,:,0:3] ** (1.0 / 2.2))
    plt.axis('off')
    plt.title('built-in AOV')
    fig.add_subplot(1, 3, 3).imshow(img_p ** (1.0 / 2.2))
    plt.axis('off')
    plt.title('built-in path tracing')
    plt.show()