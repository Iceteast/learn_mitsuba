#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex
import matplotlib.pyplot as plt


def getMSE(img, ref):
    # return the MSE of image from reference
    if len(img.array) != len(ref.array):
        print("different pic size with %d vs %d!" % (len(img.array), len(ref.array)))
        return 0.0
    ans = 0.0
    sizeOfImage = len(img.array)
    for i in range(sizeOfImage):
        ans += (img.array[i] - ref.array[i]) * (img.array[i] - ref.array[i])
    return ans / sizeOfImage

def doNothing(c):
    pass


if __name__ == '__main__':
    print("Nothing to do")
