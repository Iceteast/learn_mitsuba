#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from cfg import *
import mitsuba as mi
import drjit as dr
import matplotlib.pyplot as plt


mi.set_variant(VARIANT)

sc = mi.load_file(XMLPATH)
img = mi.render(sc, spp=SPP)

if STATUS == 'debug':
    plt.axis('off')
    plt.imshow(img ** (1.0 / 2.2))
    plt.show()
elif STATUS == 'output':
    mi.Bitmap(img).write(IMAGEPATH)
