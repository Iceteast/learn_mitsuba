#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from cfg import *
import mitsuba as mi
import drjit as dr

import matplotlib.pyplot as plt

mi.set_variant(VARIANT)

# scene = mi.load_file('models/sphere/sphere_volumetric.xml')
scene = mi.load_dict(mi.cornell_box())

cam_origin = mi.Point3f(0, 1, 3)
cam_dir = dr.normalize(mi.Vector3f(0, -0.5, -1))

cam_width = 2.0
cam_height = 2.0

image_res = [512, 512]

x, y = dr.meshgrid(
    dr.linspace(mi.Float, -cam_width / 2, cam_width / 2, image_res[0]),
    dr.linspace(mi.Float, -cam_height / 2, cam_height / 2, image_res[1])
)

ray_origin_local = mi.Vector3f(x, y, 0)

ray_origin = mi.Frame3f(cam_dir).to_world(ray_origin_local) + cam_origin

ray = mi.Ray3f(o=ray_origin, d=cam_dir)

si = scene.ray_intersect(ray)

ambient_range = 0.75
ambient_ray_count = mi.UInt32(256)

rng = mi.PCG32(size=dr.prod(image_res))

i = mi.UInt32(0)

result = mi.Float(0)

loop = mi.Loop(name="", state=lambda: (rng, i, result))

while loop(si.is_valid() & (i < ambient_ray_count)):
    sample_1, sample_2 = rng.next_float32(), rng.next_float32()
    # wo_local = mi.warp.square_to_uniform_hemisphere([sample_1, sample_2])
    wo_local = mi.warp.square_to_cosine_hemisphere([sample_1, sample_2])

    wo_world = si.sh_frame.to_world(wo_local)

    ray_2 = si.spawn_ray(wo_world)

    ray_2.maxt = ambient_range

    result[~scene.ray_test(ray_2)] += 1.0

    i += 1

result = result / ambient_ray_count

image = mi.TensorXf(result, shape=image_res)

image_origin = mi.render(scene, spp=256)

fig = plt.figure(figsize=(10, 6))
fig.subplots_adjust(wspace=0.3)

fig.add_subplot(1, 2, 1).imshow(image)
plt.axis("off")

fig.add_subplot(1, 2, 2).imshow(image_origin ** (1.0 / 2.2))
plt.axis('off')

plt.show()
