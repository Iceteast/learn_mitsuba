import mitsuba as mi
import drjit as dr
import matplotlib.pyplot as plt
import platform as pf

if pf.platform()[:7] == "Windows":
    mi.set_variant('cuda_ad_rgb')
else:
    mi.set_variant('llvm_ad_rgb')

class MyRCBSDF(mi.BSDF):

    def __init__(self, props):
        mi.BSDF.__init__(self, props)

        self.eta = props.get('eta', 1.33)
        self.alpha_u = props.get('alpha_u', 0.5)
        self.alpha_v = props.get('alpha_v', 0.7)

        reflection_flags = mi.BSDFFlags.DeltaReflection | mi.BSDFFlags.FrontSide | mi.BSDFFlags.BackSide
        transmission_flags = mi.BSDFFlags.DeltaTransmission | mi.BSDFFlags.FrontSide | mi.BSDFFlags.BackSide

        self.m_components = [reflection_flags, transmission_flags]
        self.m_flags = reflection_flags | transmission_flags

    def sample(self, ctx, si, sample1, sample2, active):

        cos_theta_i = mi.Frame3f.cos_theta(si.wi)
        r_i, cos_theta_t, eta_it, eta_ti = mi.fresnel(cos_theta_i, self.eta)
        t_i = dr.maximum(1.0 - r_i, 0.0)

        selected_r = (sample1 <= r_i) & active

        bs = mi.BSDFSample3f()
        bs.pdf = dr.select(selected_r, r_i, t_i)
        bs.sampled_component = dr.select(selected_r, mi.UInt32(0), mi.UInt32(1))
        bs.sampled_type      = dr.select(selected_r, mi.UInt32(+mi.BSDFFlags.DeltaReflection),
                                                     mi.UInt32(+mi.BSDFFlags.DeltaTransmission))
        bs.wo = dr.select(selected_r,
                          mi.reflect(si.wi),
                          mi.refract(si.wi, cos_theta_t, eta_ti))
        bs.eta = dr.select(selected_r, 1.0, eta_it)

        value_r = dr.lerp(mi.Color3f(0.3), mi.Color3f(1.0), dr.clamp(cos_theta_i, 0.0, 1.0))
        value_t = mi.Color3f(1.0) * dr.sqr(eta_ti)

        value = dr.select(selected_r, value_r, value_t)

        return bs, value

    def eval(self, ctx, si, wo, active):
        return 0.0

    def pdf(self, ctx, si, wo, active):
        return 0.0

    def eval_pdf(self, ctx, si, wo, active):
        return 0.0, 0.0

    # def traverse(self, callback):
    #     callback.put_parameter('tint', self.tint, mi.ParamFlags.Differentiable)

    def to_string(self):
        return ('MyBSDF[\n'
                '    eta=%s,\n'
                '    alpha_u=%s,\n'
                '    alpha_v=%s,\n'
                ']' % (self.eta, self.alpha_u, self.alpha_v))

if __name__ == '__main__':

    mi.register_bsdf("roughC", lambda props: MyRCBSDF(props))
   
    spp = 256
    # scene = mi.load_dict()
    scene = mi.load_file("models/cbox/cbox.xml")

    img = mi.render(scene, spp=spp)
    fig = plt.figure(figsize=(13, 7))
    fig.subplots_adjust(wspace=0.1)

    fig.add_subplot(1, 1, 1).imshow(img ** (1.0 / 2.2))
    plt.axis("off")
    plt.title('my rc')
    # fig.add_subplot(1, 2, 2).imshow(img_p ** (1.0 / 2.2))
    # plt.axis('off')
    # plt.title('built-in ')
    # fig.add_subplot(1, 3, 3).imshow((img_p - img) * 3)
    # plt.axis('off')
    # plt.title('DIFF')
    plt.show()