# Questions

## Week 1

## Week 2

## Week 3

## Week 4

I‘m still struggling on the `dr.loop`. Here are three problems which are not solved yet.

1. I can't understand how the variable `result` works in the loop. It's a float list with only one number before the loop. After the loop, it has 512*512 numbers. I tried to using a Color3f instead of it. But I can't understand its logic. 

2. I noticed there are some grammer likes a[s > 1] = 0, If it means
   
    > for all elements in a, if s > 1 then set it to 0.

I don't know how to connect the two variables `a` and `s`. Is there necessary to connect them?

3. ~~I can't get a BSDF~~

4. Do I have something wrong in the camera model? I looks different with the premade version.

5. The loop is running slower than I think. Is there something I go wrong?

## Week 5

1. LLVM is finally solved. We need to use the ver 16.0 instead of 17.1 .
   
2. MonteCarloIntegrator has a problem by initialization. Don't know why.
   
3. `sampler` function works for the whole screen, not only one ray.

## Week 7

1. ~~Simple path tracing has noise. Can't find the reason. The random number is from sampler...~~
   
2. ~~Can't find the location of anything.~~

3. In NEE, the ignored part is the direct Illumination of the sampled one? (In NEE, it's a little confused). Maybe we should keep the probability is 1.

## Week 8

1. In cpp, the eval of BSDF returns also 0.0f. KA.

## Week 10

1. Can't print some variables in loop. (`active`, `i` etc.)
2. When will the si.is_valid() be false?
3. Why print in Loop only run two times? (really 2 times? or some feature in dr.Loop)
4. Problem in `pt` seems relative with dielectric.
5. Problem in `nee` has no idea.