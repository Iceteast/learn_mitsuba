#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

# VERSION
VERSION = 'v0.1'
STATUS = 'debug'  # 'output'
# OPTIONS
VARIANT   = 'cuda_ad_rgb'
XMLPATH   = '../models/knob/mitsuba.xml'
IMAGEPATH = '../out/out.exr'
SPP = 128
GRIDLINES = True

# OUTPUT
SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768

MENU_WIDTH = 300

SLIDER_MIN = 1
SLIDER_MAX = 200

# DATA

TILE_COLOR = (153, 204, 255)  # #99CCFF
CHOOSE_COLOR = (0, 153, 102)  # #009966
GRID_COLOR = (183, 183, 183)
