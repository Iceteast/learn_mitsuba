#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from cfg import *
import mitsuba as mi
import drjit as dr
import matplotlib.pyplot as plt
import platform as pf

if pf.platform()[:7] == "Windows":
    mi.set_variant('cuda_ad_rgb')
else:
    mi.set_variant('llvm_ad_rgb')

class MyPTIntegrator(mi.SamplingIntegrator):
    
    def __init__(self, props):
        super().__init__(props)
        self.pathlength     = props.get('pathlength', 5)
        self.rr_depth       = props.get('rr_depth', 5)
        self.hide_emitters  = props.get('hide_emitters', False)

    def sample(self, scene, sampler, ray_, medium, active):
        # MI_MASKED_FUNCTION(ProfilerPhase::SamplingIntegratorSample, active);

        if (self.pathlength == 0):
            return 0.0, False

        # --------------------- Configure loop state ----------------------

        ray         = mi.Ray3f(ray_)
        throughput  = mi.Spectrum(1.0)
        result      = mi.Spectrum(0.0)
        eta         = mi.Float(1.0)
        depth       = mi.UInt(0)

        # If m_hide_emitters == false, the environment emitter will be visible

        valid_ray = mi.Bool((not self.hide_emitters) and dr.neq(scene.environment(), None))

        # Variables caching information from the previous bounce
        prev_si         = dr.zeros(mi.SurfaceInteraction3f)
        prev_bsdf_pdf   = mi.Float(1.0)
        prev_bsdf_delta = mi.Bool(True)
        bsdf_ctx        = mi.BSDFContext()

        ''' Set up a Dr.Jit loop. This optimizes away to a normal loop in scalar
           mode, and it generates either a a megakernel (default) or
           wavefront-style renderer in JIT variants. This can be controlled by
           passing the '-W' command line flag to the mitsuba binary or
           enabling/disabling the JitFlag.LoopRecord bit in Dr.Jit.

           The first argument identifies the loop by name, which is helpful for
           debugging. The subsequent list registers all variables that encode
           the loop state variables. This is crucial: omitting a variable may
           lead to undefined behavior. '''
        loop = mi.Loop("Path Tracer", state= lambda : (sampler, ray, throughput, result,
                            eta, depth, valid_ray, prev_si, prev_bsdf_pdf,
                            prev_bsdf_delta, active))

        ''' Inform the loop about the maximum number of loop iterations.
           This accelerates wavefront-style rendering by avoiding costly
           synchronization points that check the 'active' flag. '''
        loop.set_max_iterations(self.pathlength)

        while (loop(active)):
            '''/* dr::Loop implicitly masks all code in the loop using the 'active'
                flag, so there is no need to pass it to every function */'''

            si = scene.ray_intersect(ray, +mi.RayFlags.All, dr.eq(depth, mi.UInt(0)))   # what is the plus mean?

            # ---------------------- Direct emission ----------------------

            '''/* dr::any_or() checks for active entries in the provided boolean
                array. JIT/Megakernel modes can't do this test efficiently as
                each Monte Carlo sample runs independently. In this case,
                dr::any_or<..>() returns the template argument (true) which means
                that the 'if' statement is always conservatively taken. */'''
            
            
            ds = mi.DirectionSample3f(scene, si, prev_si)
            em_pdf = mi.Float(0.0)
            
            not_emitter = dr.neq(si.emitter(scene), None)
            mask_em = not_emitter & ~prev_bsdf_delta
            em_pdf[mask_em] = scene.pdf_emitter_direction(prev_si, ds, ~prev_bsdf_delta)

            # Compute MIS weight for emitter sample from previous bounce
            mis_bsdf = self.mis_weight(prev_bsdf_pdf, em_pdf)

            # Accumulate, being careful with polarization (see spec_fma)
            result[not_emitter] = throughput * ds.emitter.eval(si, prev_bsdf_pdf > mi.Float(0)) * mis_bsdf + result

            # Continue tracing the path at this point?

            active_next = (depth + 1 < self.pathlength) & si.is_valid()
            # if (~dr.any(active_next)): # <False>
            #     break  # early exit for scalar mode

            bsdf = si.bsdf(ray)

            # ---------------------- Emitter sampling ----------------------
            
            # Perform emitter sampling?
            active_em = active_next & mi.has_flag(bsdf.flags(), mi.BSDFFlags.Smooth)

            # ds = dr.zeros(mi.DirectionSample3f)
            # em_weight = dr.zeros(mi.Spectrum)
            # wo = dr.zeros(mi.Vector3f)

            # if (dr.any(active_em)):
                # Sample the emitter
            ds, em_weight = scene.sample_emitter_direction(si, sampler.next_2d(), True, active_em)
            active_em &= dr.neq(ds.pdf, mi.Float(0))

            '''/* Given the detached emitter sample, recompute its contribution
                with AD to enable light source optimization. */'''
            if (dr.grad_enabled(si.p)):
                ds.d = dr.normalize(ds.p - si.p)
                em_val = scene.eval_emitter_direction(si, ds, active_em)
                em_weight = dr.select(dr.neq(ds.pdf, mi.Float(0)), em_val / ds.pdf, mi.Float(0))
                

            wo = si.to_local(ds.d)

            # ------ Evaluate BSDF * cos(theta) and sample direction -------

            sample_1 = sampler.next_1d()
            sample_2 = sampler.next_2d()

            bsdf_val, bsdf_pdf, bsdf_sample, bsdf_weight = bsdf.eval_pdf_sample(bsdf_ctx, si, wo, sample_1, sample_2)
            
            # --------------- Emitter sampling contribution ----------------

            # if (dr.any(active_em)):
            bsdf_val = si.to_world_mueller(bsdf_val, -wo, si.wi)

            # Compute the MIS weight
            mis_em = dr.select(ds.delta, mi.Float(1.0), self.mis_weight(ds.pdf, bsdf_pdf))

            # Accumulate, being careful with polarization (see spec_fma)
            result[active_em] = throughput * (bsdf_val * em_weight * mis_em) + result
            

            # ---------------------- BSDF sampling ----------------------

            bsdf_weight = si.to_world_mueller(bsdf_weight, -bsdf_sample.wo, si.wi)

            ray = si.spawn_ray(si.to_world(bsdf_sample.wo))

            '''/* When the path tracer is differentiated, we must be careful that
                the generated Monte Carlo samples are detached (i.e. don't track
                derivatives) to avoid bias resulting from the combination of moving
                samples and discontinuous visibility. We need to re-evaluate the
                BSDF differentiably with the detached sample in that case. */'''
            if (dr.grad_enabled(ray)):
                ray = dr.detach(ray)

                # Recompute 'wo' to propagate derivatives to cosine term
                wo_2 = si.to_local(ray.d)
                bsdf_val_2, bsdf_pdf_2 = bsdf.eval_pdf(bsdf_ctx, si, wo_2, active)
                bsdf_weight[bsdf_pdf_2 > mi.Float(0.0)] = bsdf_val_2 / dr.detach(bsdf_pdf_2)
            

            # ------ Update loop variables based on current interaction ------

            throughput *= bsdf_weight
            eta *= bsdf_sample.eta
            valid_ray |= active & si.is_valid() & ~mi.has_flag(bsdf_sample.sampled_type, mi.BSDFFlags.Null)

            # Information about the current vertex needed by the next iteration
            prev_si = si
            prev_bsdf_pdf = bsdf_sample.pdf
            prev_bsdf_delta = mi.has_flag(bsdf_sample.sampled_type, mi.BSDFFlags.Delta)

            # -------------------- Stopping criterion ---------------------

            depth[si.is_valid()] += 1

            throughput_max = dr.max(mi.unpolarized_spectrum(throughput))

            rr_prob = dr.minimum(throughput_max * dr.sqr(eta), mi.Float(0.95))
            rr_active = depth >= self.rr_depth
            rr_continue = sampler.next_1d() < rr_prob

            '''/* Differentiable variants of the renderer require the the russian
                roulette sampling weight to be detached to avoid bias. This is a
                no-op in non-differentiable variants. */'''
            throughput[rr_active] *= dr.rcp(dr.detach(rr_prob))

            active = (active_next & (~rr_active | rr_continue)) & dr.neq(throughput_max, mi.Float(0.0))


        return dr.select(valid_ray, result, mi.Float(0.0)), valid_ray, []

    def mis_weight(self, pdf_a, pdf_b):
        pdf_a *= pdf_a
        pdf_b *= pdf_b
        w = pdf_a / (pdf_a + pdf_b)
        return dr.detach(dr.select(dr.isfinite(w), w, mi.Float(0)))


if __name__ == '__main__':

    mi.register_integrator("mypt", lambda props: MyPTIntegrator(props))
    o = 'o'
    spp = 256
    # scene = mi.load_dict(mi.cornell_box())
    # scene = mi.load_file("xmls/cbox/cbox.xml")
    # scene = mi.load_file("models/living-room-2/scene_v3.xml")
    scene = mi.load_file("xmls/staircase2/scene_v3.xml")

    pt = mi.load_dict({
        'type' : 'path'
    })

    my = mi.load_dict({
        'type' : 'mypt',
        'pathlength' : 7,
    })

    img = mi.render(scene, integrator=my, spp=spp)
    img_p = mi.render(scene, integrator=pt, spp=spp)
    if (o == 'o'):
        mi.Bitmap(img).write("my.exr")
        mi.Bitmap(img_p).write("pt.exr")
    
    fig = plt.figure(figsize=(13, 7))
    fig.subplots_adjust(wspace=0.1)

    fig.add_subplot(1, 3, 1).imshow(img ** (1.0 / 2.2))
    plt.axis("off")
    plt.title('my pt')
    fig.add_subplot(1, 3, 2).imshow(img_p ** (1.0 / 2.2))
    plt.axis('off')
    plt.title('built-in path tracing')
    fig.add_subplot(1, 3, 3).imshow(abs(img_p - img))
    plt.axis('off')
    plt.title('DIFF')
    plt.show()