import drjit as dr
from drjit.cuda import Float, UInt, Array3f, Array2f, TensorXf, Texture3f, PCG32, Loop


def sdf(p: Array3f) -> Float:

    return dr.norm(p) - 1


if __name__ == "__main__":
    print(sdf(Array3f(1, 2, 3)))
