#! /usr/bin/env python3
# -*- coding:utf-8 -*-
# %%
from cfg import *
import mitsuba as mi
import drjit as dr
import matplotlib.pyplot as plt
import platform as pf
# %%
if pf.platform()[:7] == "Windows":
    mi.set_variant('cuda_ad_rgb')
else:
    mi.set_variant('llvm_ad_rgb')
# %%
class MyNEEPTIntegrator(mi.SamplingIntegrator):
    
    def __init__(self, props):
        super().__init__(props)
        self.pathlength = props.get('pathlength', 5)

    def sample(self, scene, sampler, ray, medium, active, **kwargs):
        color_acc = mi.Spectrum(1)
        result = mi.Spectrum(0)

        ctx =  mi.BSDFContext()
        i = mi.Int(0)
        next_ray = mi.Ray3f(ray)
        loop = mi.Loop("myNEE", state=lambda : (i, next_ray, active))
        loop.set_max_iterations(self.pathlength)

        while loop(active):
            print("loop")
            si:mi.SurfaceInteraction3f = scene.ray_intersect(next_ray)
            active = si.is_valid() & active
            
            # only direct luminance
            result[dr.neq(si.emitter(scene), None) & dr.eq(i, 0)] = color_acc * si.emitter(scene).eval(si) + result
            
            # sample light
            ds, color = scene.sample_emitter_direction (si, sampler.next_2d(), True, active)
            
            active[dr.eq(si.emitter(scene), None)] = False
            result[active] = color_acc * color + result
            # BSDF
            # bsdf_val, bsdf_pdf, bs, bsdf_weight = si.bsdf().eval_pdf_sample(ctx, si, ds.d, sampler.next_1d(), sampler.next_2d())
            # color_acc[bsdf_pdf > 0] *= bsdf_val / bsdf_pdf
            bs, value = si.bsdf().sample(ctx, si, sampler.next_1d(), sampler.next_2d(), active)
            pdf_bsdf = si.bsdf().pdf(ctx, si, bs.wo)
            color_acc *= value / pdf_bsdf

            next_ray = si.spawn_ray(si.sh_frame.to_world(bs.wo))
            i += 1

        return result, active, []

    # def sample(self, scene, sampler, ray_, medium, active):
    #     # MI_MASKED_FUNCTION(ProfilerPhase::SamplingIntegratorSample, active);

    #     if (self.pathlength == 0):
    #         return 0.0, False

    #     # --------------------- Configure loop state ----------------------

    #     ray         = mi.Ray3f(ray_)
    #     throughput  = mi.Spectrum(1.0)
    #     result      = mi.Spectrum(0.0)
    #     eta         = mi.Float(1.0)
    #     depth       = mi.UInt(0)

    #     # If m_hide_emitters == false, the environment emitter will be visible

    #     # Variables caching information from the previous bounce
    #     prev_si         = dr.zeros(mi.SurfaceInteraction3f)
    #     prev_bsdf_pdf   = mi.Float(1.0)
    #     prev_bsdf_delta = mi.Bool(True)
    #     bsdf_ctx        = mi.BSDFContext()

    #     loop = mi.Loop("Path Tracer", state= lambda : (sampler, ray, throughput, result,
    #                         eta, depth, prev_si, prev_bsdf_pdf,
    #                         prev_bsdf_delta, active))

    #     loop.set_max_iterations(self.pathlength)

    #     while (loop(active)):
    #         print("Loop")
    #         si = scene.ray_intersect(ray, +mi.RayFlags.All, dr.eq(depth, mi.UInt(0)))   # what is the plus mean?

    #         # ---------------------- Direct emission ----------------------
            
    #         ds = mi.DirectionSample3f(scene, si, prev_si)
    #         em_pdf = mi.Float(0.0)
            
    #         not_emitter = dr.neq(si.emitter(scene), None)
    #         mask_em = not_emitter & ~prev_bsdf_delta
    #         em_pdf[mask_em] = scene.pdf_emitter_direction(prev_si, ds, ~prev_bsdf_delta)

    #         # Accumulate, being careful with polarization (see spec_fma)
    #         result[~not_emitter] = throughput * ds.emitter.eval(si, prev_bsdf_pdf > mi.Float(0)) * em_pdf + result

    #         # Continue tracing the path at this point?

    #         active_next = (depth + 1 < self.pathlength) & si.is_valid()

    #         bsdf = si.bsdf(ray)

    #         # ---------------------- Emitter sampling ----------------------
            
    #         # Perform emitter sampling?
    #         active_em = active_next & mi.has_flag(bsdf.flags(), mi.BSDFFlags.Smooth) # ignore Spectular bounces.

    #         # if (dr.any(active_em)):
    #             # Sample the emitter
    #         ds, em_weight = scene.sample_emitter_direction(si, sampler.next_2d(), True, active_em)
    #         active_em &= dr.neq(ds.pdf, mi.Float(0))

    #         wo = si.to_local(ds.d)

    #         # ------ Evaluate BSDF * cos(theta) and sample direction -------

    #         sample_1 = sampler.next_1d()
    #         sample_2 = sampler.next_2d()

    #         bsdf_val, bsdf_pdf, bsdf_sample, bsdf_weight = bsdf.eval_pdf_sample(bsdf_ctx, si, wo, sample_1, sample_2)
            
    #         # --------------- Emitter sampling contribution ----------------

    #         # if (dr.any(active_em)):
    #         bsdf_val = si.to_world_mueller(bsdf_val, -wo, si.wi)

    #         # Compute the MIS weight

    #         # Accumulate, being careful with polarization (see spec_fma)
    #         result[active_em] = throughput * (bsdf_val * em_weight * bsdf_pdf) + result
            

    #         # ---------------------- BSDF sampling ----------------------

    #         bsdf_weight = si.to_world_mueller(bsdf_weight, -bsdf_sample.wo, si.wi)

    #         ray = si.spawn_ray(si.to_world(bsdf_sample.wo))

    #         # ------ Update loop variables based on current interaction ------

    #         throughput *= bsdf_weight
    #         eta *= bsdf_sample.eta

    #         # Information about the current vertex needed by the next iteration
    #         prev_si = si
    #         prev_bsdf_pdf = bsdf_sample.pdf
    #         prev_bsdf_delta = mi.has_flag(bsdf_sample.sampled_type, mi.BSDFFlags.Delta)

    #         # -------------------- Stopping criterion ---------------------
    #         active = active_next


    #     return dr.select(True, result, mi.Float(0.0)), True, []

# %%
if __name__ == '__main__':
    mi.register_integrator("mynee", lambda props: MyNEEPTIntegrator(props))
    # o = 'o'
    o = ''
    spp = 1024
    scene = mi.load_dict(mi.cornell_box())
    # scene = mi.load_file("xmls/cbox/cbox.xml")

    my = mi.load_dict({
        'type' : 'mynee',
        'pathlength' : 7,
    })

    pt = mi.load_dict({
        'type' : 'path'
    })

    img = mi.render(scene, integrator=my, spp=spp)
    img_p = mi.render(scene, integrator=pt, spp=spp)
    if (o == 'o'):
        mi.Bitmap(img).write("my.exr")
        mi.Bitmap(img_p).write("pt.exr")

    fig = plt.figure(figsize=(13, 7))
    fig.subplots_adjust(wspace=0.1)

    fig.add_subplot(1, 3, 1).imshow(img ** (1.0 / 2.2))
    plt.axis("off")
    plt.title('my pt')
    fig.add_subplot(1, 3, 2).imshow(img_p ** (1.0 / 2.2))
    plt.axis('off')
    plt.title('built-in path tracing')
    fig.add_subplot(1, 3, 3).imshow((img_p - img) ** (1.0 / 2.2))
    plt.axis('off')
    plt.title('DIFF')
    plt.show()
    print((img_p - img).array[1], img_p.array[1], img.array[1])
# %%
