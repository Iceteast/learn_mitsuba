#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from cfg import *
from utils import getMSE
import mitsuba as mi
import drjit as dr
import matplotlib.pyplot as plt

from BSDFs.myBSDF import MyBSDF

def getMSEImage(img, ref, color='color'):
    if len(img.array) != len(ref.array):
        raise Exception("different pic size with %d vs %d!" % (len(img.array), len(ref.array)))

    res = dr.zeros(dtype=mi.Float, shape=512*512*3)

    if color == 'color':
        ans = 0.0
        for i in range(len(img.array)): # TODO: change it into mi.Loop
            ans += abs((img.array[i] - ref.array[i]))
            if i % 3 == 2:
                res[i], res[i-1], res[i-2] = ans, ans, ans
                ans = 0.0

    elif color == 'gray':
        res[i] += abs((img.array[i] - ref.array[i]))
    else:
        raise Exception("unexpected color set")
    
    return mi.TensorXf(res, shape=img.shape)

def output(img, ref, opts='MSEON'):
    fig = plt.figure(figsize=(10, 6))
    fig.subplots_adjust(wspace=0.1)

    fig.add_subplot(1, 3, 1).imshow(img)
    plt.axis("off")
    plt.title('my output')
    fig.add_subplot(1, 3, 2).imshow(ref ** (1.0 / 2.2))
    plt.axis('off')
    plt.title('standard render')
    if opts == 'MSEON':
        mse_img = getMSEImage(img, ref)
        plt.suptitle('MSE=%.5f' % (getMSE(img, ref)))
        fig.add_subplot(1, 3, 3).imshow(mse_img ** (1.0 / 2.2))
        plt.axis('off')
        plt.title('difference')
    plt.show()


mi.set_variant(VARIANT)

# scene = mi.load_file('../models/sphere/sphere_volumetric.xml')
scene = mi.load_dict(mi.cornell_box())

cam_origin = mi.Point3f(0, 1, 3)
cam_dir = dr.normalize(mi.Vector3f(0, -0.5, -1.5))

cam_width = 2.0
cam_height = 2.0

image_res = [512, 512]

x, y = dr.meshgrid(
    dr.linspace(mi.Float, -cam_width  / 2, cam_width  / 2, image_res[0]),
    dr.linspace(mi.Float, -cam_height / 2, cam_height / 2, image_res[1])
)

ray_origin_local = mi.Vector3f(x, y, 0)

ray_dir = mi.Frame3f(cam_dir).to_world(ray_origin_local) - cam_origin

ray = mi.Ray3f(o=cam_origin, d=ray_dir)

si : mi.SurfaceInteraction3f = scene.ray_intersect(ray)

ctx = mi.BSDFContext()

ambient_range = 0.75
maximal_ray_count = mi.UInt32(200)

rng = mi.PCG32(size=dr.prod(image_res[0:2]))
i = mi.UInt32(0)
result = mi.Float32(0)

loop = mi.Loop(name="MyLoop", state=lambda: (si, i, result))
# print(scene.lights)
while loop(si.is_valid() & (i < maximal_ray_count)):
    #sample 
    # si : mi.SurfaceInteraction3f = scene.ray_intersect(ray)                   si in loop leads to a colorless output why?
    sample_1, sample_2 = rng.next_float32(), rng.next_float32()

    wo_local = mi.warp.square_to_uniform_hemisphere([sample_1, sample_2])
    wo_world = si.sh_frame.to_world(wo_local)
    ray_2 = si.spawn_ray(wo_world)
    result[~scene.ray_test(ray_2)] += 1 # dr.cuda.Array3f(0, 0, 1)
    
    i += 1

result = result / maximal_ray_count

image = mi.TensorXf(result, shape=image_res)

params = mi.traverse(scene)
params['sensor.to_world'] = mi.ScalarTransform4f.look_at(origin=[0, 1, 3],
                                                         target=[0, -0.5, -1],
                                                         up=[0, 0, -1])
params['sensor.film.size'] = mi.ScalarVector2u(512, 512)
params.update()

image_origin = mi.render(scene, spp=256)

output(image, image_origin, 'MSEOFF')
